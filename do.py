import numpy as np
from scipy.optimize import fsolve
import matplotlib.pyplot as plt


#def dist(x0,y0,x1,y1):
#  return sym.sqrt((x0-x1)**2+(y0-y1)**2)

def dist((x0,y0),(x1,y1)):
  return np.sqrt((x0-x1)**2+(y0-y1)**2)

s = [(0,0), (1,0), (0,1)]
d = [ [5**0.5-2**0.5, 5**0.5-2**0.5], [2**0.5-1, 1], [1, 2**0.5-1] ]

s = [(16,13), (25,199), (262,11)]
d = [ (21,50), (-26, -45.5), (2, -11) ]

def myFunction(z):
  p = [z[0:2],z[2:4],z[4:6]]
  f = np.empty(6)
  for t in [1,2]:
    for i in [0,1,2]:
      f[i*2+t-1] = dist(p[t],s[i]) - dist(p[0],s[i]) - d[i][t-1]
  return f

zGuess = np.array([0,0,1,1,2,2])
z = fsolve(myFunction,zGuess)
p1 = [z[0:2],z[2:4],z[4:6]]

zGuess = np.array([20,20,20,20,20,20])
z = fsolve(myFunction,zGuess)
p = [z[0:2],z[2:4],z[4:6]]


print(p)

for i in [0,1,2]:
    plt.plot(s[i][0],s[i][1],'r*',label='')
    plt.plot(p[i][0],p[i][1],'bo',label='')
    plt.plot(p1[i][0],p1[i][1],'go',label='')
plt.xlabel('')
plt.ylabel('')
plt.title('')
#plt.legend(loc='bottom right')
plt.show()
